import React, { useState } from 'react';
import RiskBar from './risk-bar.js';
import DataTable from './data-table.js';
import DataChart from './data-chart.js';

function Dashboard (){

    const [mode, setMode] = useState("table");
    
    return (
        <div className="dashboard">
                <RiskBar/>

                {mode === "table" && (
                    <div className="tableMode">
                        <div className="row">           
                            <button onClick={() => setMode('chart') }>Show graph</button>
                        </div>
                        <DataTable />
                    </div>
                )}

                {mode === "chart" && (
                    <div className="tableMode">
                        <div className="row">           
                            <button onClick={() => setMode('table') }>Show table</button>
                        </div>
                        <DataChart />
                    </div>
                )}
        </div>
    );
}

export default Dashboard