import React from 'react';
import Dashboard from './dashboard.js';
import Header from './header.js';
import Calculator from './calculator.js'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App (){

    return (
        <Router>
            <Header />
            <Switch>
                <Route path="/" exact component={ Dashboard } />
                <Route path="/calculator" component={Calculator} />
            </Switch>
        </Router>
    );

}

export default App;