import React from 'react';
import './../styles/calculator.css';
import RiskRow from './risk-row.js';
import RiskFolio from './risk-folio.js';

function Calculator(){

    return (
        <div className="row">
            <RiskRow />
            <RiskFolio />
        </div>)
}

export default Calculator