import React from 'react';
import './../styles/data-table.css'
import { useSelector } from 'react-redux';

function DataTable(){

    const data = useSelector(state => state);

    return (
        <div id="tableSpace" className="row">

            <table className="main">
                <thead>
                    <tr>
                        <th>Risk</th>
                        <th>Bonds %</th>
                        <th>Large Cap %</th>
                        <th>Mid Cap %</th>
                        <th>Foreign %</th>
                        <th>Small Cap%</th>
                    </tr>
                </thead>
                <tbody>
                    { addData(data) }
                </tbody>
            </table>
        </div>
    );

}       

const addData = (data) => {
    return data.payload.map((row, index) => {
        return  (<tr key={row.risk} className={data.data.risk === row.risk ? "selected": "" }>
                <td>{row.risk}</td>
                <td>{row.bonds}</td>
                <td>{row.largeCap}</td>
                <td>{row.midCap}</td>
                <td>{row.foreign}</td>
                <td>{row.smallCap}</td>
            </tr>)
    });
}

export default DataTable