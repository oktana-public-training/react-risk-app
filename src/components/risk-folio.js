import React, { useState } from 'react';
import { useSelector } from 'react-redux';

function RiskFolio(){
    let letDiferences= {bonds: "", largeCap: "", midCap:"", foreign:"", smallCap:""};
    let letNewAmounts = {bonds: "", largeCap: "", midCap:"", foreign:"", smallCap:""};
    const [input, setInput] = useState({bonds: "", largeCap: "", midCap:"", foreign:"", smallCap:""});
    const [results, setResults] = useState({inputs: input, diferences: letDiferences, newAmounts: letNewAmounts, suggestion:""});
    const [active, setActive] = useState(false);
    const row = useSelector(state => state.data);

    const handleChange = event => {
        const { name, value } = event.target;
        setInput({
            ...input,
            [name]: Number(value)
        });
        setActive(true);
    }

    const calculate = () => {

        var subtotal = 0;
        for(const key in input){
            subtotal = subtotal + input[key];
        }

        letNewAmounts = {
            bonds: subtotal * row.bonds / 100,
            largeCap: subtotal * row.largeCap / 100,
            midCap: subtotal * row.midCap / 100,
            foreign: subtotal * row.foreign / 100,
            smallCap: subtotal * row.smallCap / 100
        };

        letDiferences = {
            bonds: Math.round((letNewAmounts.bonds - input.bonds) * 100)/ 100,
            largeCap: Math.round((letNewAmounts.largeCap - input.largeCap) * 100)/ 100,
            midCap: Math.round((letNewAmounts.midCap - input.midCap) * 100)/ 100,
            foreign: Math.round((letNewAmounts.foreign - input.foreign) * 100)/ 100,
            smallCap: Math.round((letNewAmounts.smallCap - input.smallCap) * 100)/ 100
        }
        console.log(letDiferences);
        const suggestions = getSugestions(letDiferences);
        setResults({
            inputs: input,
            diferences: letDiferences,
            newAmounts: letNewAmounts,
            suggestion: suggestions
        })
        
    }

    const getSugestions = (prop)=> {

        const substracts = {};
        const additions = {};
        let suggestion = '';
        for(const key in prop){
            if(prop[key] > 0){
                additions[key] = prop[key];
            }else if(prop[key] < 0){
                substracts[key] = prop[key]
            }
        }

        for(const plusKey in additions){
            for(const minuskey in substracts){
                if(additions[plusKey] > substracts[minuskey] && substracts[minuskey] !== 0){
                    suggestion += ' Transfer ' + ((  substracts[minuskey]) * -1) + " from " + minuskey + " to " + plusKey + ".-";
                    additions[plusKey] += substracts[minuskey] ;
                    substracts[minuskey] = 0;
                }else if(additions[plusKey] > 0){
                    suggestion += ' Transfer ' + (additions[plusKey]) + " from " + minuskey + " to " + plusKey  + ".";
                    additions[plusKey] = 0;
                    break;
                }
            }
        }
        return suggestion
    }

    return (
        <div className="row">
            <div className="body-title"> 
                <strong>Please Enter Your Current Portfolio</strong>

                <div className={ active? "button": "button inactive"} onClick={ calculate }>
                    Rebalance
                </div>
            </div>
            <table className="table-form">
                <thead>
                    <tr>
                        <th colSpan="2">Current Amount</th>
                        <th>Difference</th>
                        <th>New Amount</th>
                        <th>Recomented Transfers</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Bonds $:</td>
                        <td><input type="text" name="bonds" onChange={ handleChange } /></td>
                        <td><input type="text" readOnly="readonly" value={ results.diferences.bonds } className={ results.diferences.bonds >= 0?"positive":"negative"}/></td>
                        <td><input type="text" readOnly="readonly" value={ results.newAmounts.bonds } className="result" /></td>
                        <td rowSpan="5">{ results.suggestion }</td>
                    </tr>
                    <tr>
                        <td>Large Cap $:</td>
                        <td><input type="text" name="largeCap" onChange={ handleChange } /></td>
                        <td><input type="text" readOnly="readonly" value={  results.diferences.largeCap } className={results.diferences.largeCap >= 0?"positive":"negative"}/></td>
                        <td><input type="text" readOnly="readonly"  value={  results.newAmounts.largeCap } className="result"/></td>
                    </tr>
                    <tr>
                        <td>Mid Cap $:</td>
                        <td><input type="text" name="midCap" onChange={ handleChange } /></td>
                        <td><input type="text" readOnly="readonly" value={ results.diferences.midCap } className={results.diferences.midCap >= 0?"positive":"negative"}/></td>
                        <td><input type="text" readOnly="readonly"  value={ results.newAmounts.midCap } className="result"/></td>
                    </tr>
                    <tr>
                        <td>Foreign $:</td>
                        <td><input type="text" name="foreign" onChange={ handleChange } /></td>
                        <td><input type="text" readOnly="readonly" value={ results.diferences.foreign } className={results.diferences.foreign >= 0?"positive":"negative"}/></td>
                        <td><input type="text" readOnly="readonly" value={ results.newAmounts.foreign } className="result"/></td>
                    </tr>
                    <tr>
                        <td>Small Cap $:</td>
                        <td><input type="text" name="smallCap" onChange={ handleChange } /></td>
                        <td><input type="text" readOnly="readonly" value={ results.diferences.smallCap } className={results.diferences.smallCap >= 0?"positive":"negative"}/></td>
                        <td><input type="text" readOnly="readonly" value={ results.newAmounts.smallCap } className="result"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default RiskFolio;