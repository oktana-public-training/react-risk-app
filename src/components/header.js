import React from 'react'
import './../styles/header.css'
import home from './../img/home.png';
import { Link } from 'react-router-dom';

export default class Header extends React.Component{

    render(){
        return (
            <header>
                <Link to="/">
                    <img src={home} alt="home" />
                </Link>
                <div className="title">Financial Advisor</div>
            </header>
        );

    }

}