import React from 'react';
import { useSelector } from 'react-redux';
import { PieChart } from 'react-minimal-pie-chart';
import './../styles/data-chart.css';

function DataChart(){

    const data = useSelector(state => state.data);

    const buildChardData = (data) => {

        var response = [{
            title: 'No selected',
            value: 0,
            color: '#FFF'
        }];

        if(data.risk){
            response = [
                {
                    title: "Bonds",
                    value: data.bonds,
                    color: "#FF5733"
                },
                {
                    title: "Large Cap",
                    value: data.largeCap,
                    color: "#33FF7A"
                },
                {
                    title: "Mid Cap",
                    value: data.midCap,
                    color: "#33DAFF"
                },
                {
                    title: "Foreign",
                    value: data.foreign,
                    color: "#5233FF"
                },
                {
                    title: "Small Cap",
                    value: data.smallCap,
                    color: "#FF3393"
                }           
            ];
        }
        return response;

    }

    var chartData = buildChardData(data);

    return  <div id="chartSpace">
                <PieChart 
                style={{
                    fontFamily:
                    '"Nunito Sans", -apple-system, Helvetica, Arial, sans-serif',
                    fontSize: '2px',
                }}
                data={chartData} 
                radius={PieChart.defaultProps.radius - 6}
                lineWidth={60}
                animate
                label={({ dataEntry }) => dataEntry.title+' ' + Math.round(dataEntry.percentage) + '%'}
                labelPosition={100 - 60 / 2}
                labelStyle={{
                fill: '#fff',
                pointerEvents: 'none',
                size: '30px'
                }}
                />
            </div>

}



export default DataChart;