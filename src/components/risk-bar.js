import React from 'react';
import './../styles/risk-bar.css';
import { useSelector, useDispatch } from 'react-redux';
import { select } from './../actions'; 
import { Link } from 'react-router-dom';

function RiskBar(){


    const data = useSelector(state => state.data);
    const payload = useSelector(state => state.payload);
    const dispatch = useDispatch();

    const createList = (loops) => {
        let list = [];
        var isActive = data.risk != null;
        for (let i = 0; i < loops; i++) {
            list.push(<li key={i+1} className={ data.risk === i+1?"selected":""} onClick={ () => selectData(i+1) }>{ i+1 }</li>)
            
        }
        list.push(
                <li key="continue" style={isActive? { cursor: "pointer" } : { opacity: 0.5, cursor: "auto" } } >
                    <Link to="/calculator" 
                        style={isActive? { cursor: "pointer" } : { opacity: 0.5, cursor: "auto" } }
                        onClick={ (event) => !isActive? event.preventDefault() :true }>
                        Continue
                    </Link>
                </li>
        );
        return list;
    }

    const selectData = (index) => {
        var row = payload.find(e => e.risk === index);
        dispatch(select(row));
    }

    return (
        <div className="row">
            <div className="risk-label">Please Select A Risk Level For Your Investment Portfolio</div>
            <div className="risk-label-levels">
                <span className="risk-level">Low</span>
                <span className="risk-level">High</span>
            </div>
            <div className="risk-levels">
                <ul>
                    { createList(10) }
                </ul>
            </div>
        </div>
    );
}

export default RiskBar;