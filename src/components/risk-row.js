import React from 'react';
import { useSelector } from 'react-redux';

function RiskRow(){

    const row = useSelector(state => state.data);
    
    return (
        <div className="row">
            <h1> Personalized Portafolio </h1>
            <div className="body-title"> 
                <strong>Risk Level { row.risk }</strong>
            </div>
            <table>
            <thead>
                <tr>
                    <th>Bonds %</th>
                    <th>Large Cap %</th>
                    <th>Mid Cap %</th>
                    <th>Foreign %</th>
                    <th>Small Cap%</th>
                </tr>
            </thead>
            <tbody>
                { displayData(row) }
            </tbody>
            </table>
        </div>
    );
}

const displayData = function(data) {
    return  (<tr>
            <td>{data.bonds}%</td>
            <td>{data.largeCap}%</td>
            <td>{data.midCap}%</td>
            <td>{data.foreign}%</td>
            <td>{data.smallCap}%</td>
        </tr>)
}

export default RiskRow;