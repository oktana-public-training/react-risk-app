import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './components/app.js';
import allReducers from './reducers/index.js';
import {createStore} from 'redux';
import { Provider } from 'react-redux';

const store = createStore(allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store} >
        <App />
    </Provider>,
    document.getElementById('root')
);