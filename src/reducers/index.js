import rowReducer from './rowReducer.js';
import dataReducer from './dataReducer.js';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    data: rowReducer,
    payload: dataReducer
})

export default allReducers